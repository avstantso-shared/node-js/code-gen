const { resolve, log } = require('./utils');

const ArrowFunc = require('./arrow-func');
const Const = require('./const');
const Function = require('./function');
const Interface = require('./interface');
const Namespace = require('./namespace');
const Object = require('./object');
const Type = require('./type');

const object0 = Object();

function Default(prefix = '') {
  const p = `${prefix || ''}default `;

  function def(content) {
    return log('Default', { prefix, content }) && `${p}${resolve(content)}`;
  }

  def.Object = Object(p);
  def.Function = Function(p);
  def.ArrowFunc = ArrowFunc(p);

  return def;
}

module.exports = (prefix = '') => {
  const p = `${prefix || ''}export `;

  function exp(content) {
    function f() {
      return (
        log('Export', { prefix, content }) &&
        `${p}${resolve(content).trimEnd()};`
      );
    }
    f.From = (file) =>
      log('Export.From', { prefix, content, file }) &&
      `${p}${content} from '${resolve(file)}';\r\n`;

    return f;
  }

  function expObj(...members) {
    return exp(object0(resolve(members, ',')));
  }

  exp.Asterisk = { From: exp('*').From };
  exp.Members = (...members) => ({
    From: expObj(...members).From,
  });
  exp.Object = (...members) => expObj(...members)();

  exp.Const = Const(p);
  exp.Default = Default(p);
  exp.Function = Function(p);
  exp.Interface = Interface(p);
  exp.Namespace = Namespace(p);
  exp.Type = Type(p);

  return exp;
};
