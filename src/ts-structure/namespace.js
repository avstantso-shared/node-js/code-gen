const { resolve, log } = require('./utils');
const Block = require('./block');

module.exports = (prefix = '') => {
  const block = Block(prefix);

  function ns(name, content) {
    return (
      log('Namespace', { prefix, name, content }) &&
      block(`namespace ${resolve(name)} {`, '}', content)
    );
  }

  return ns;
};
