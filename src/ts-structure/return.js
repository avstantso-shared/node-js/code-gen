const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (content) => {
    return (
      log('Return', { prefix, content }) &&
      `${prefix}return ${resolve(content)}`
    );
  };
};
