const { resolve, log } = require('./utils');
const Block = require('./block');

module.exports = (prefix = '') => {
  const block = Block(prefix);
  return (name, ext, content) =>
    log('Interface', { prefix, name, ext, content }) &&
    block(
      `interface ${resolve(name)}${ext ? ` extends ${resolve(ext)}` : ''} {`,
      '}',
      content
    );
};
