const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (candidate, template, ifExtends, ifNotExtends) => {
    return (
      log('Extends', {
        prefix,
        candidate,
        template,
        ifExtends,
        ifNotExtends,
      }) &&
      `${prefix}${resolve(candidate)} extends ${resolve(template)} ? ${resolve(
        ifExtends
      )} : ${resolve(ifNotExtends)}`
    );
  };
};
