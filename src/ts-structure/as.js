const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (content, type) => {
    return (
      log('As', { prefix, content }) &&
      `${prefix}${resolve(content)} as ${resolve(type)}`
    );
  };
};
