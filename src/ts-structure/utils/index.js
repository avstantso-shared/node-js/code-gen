const resolve = require('./resolve');
const concat = require('./concat');
const log = require('./log');

module.exports = { resolve, concat, log };
