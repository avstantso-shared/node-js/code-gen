const resolve = require('./resolve');

function Concat(...items) {
  const resolved = items.map(resolve).filter((item) => item);
  return !resolved.some((item) => !!item.trim()) ? undefined : resolved;
}

module.exports = Concat;
