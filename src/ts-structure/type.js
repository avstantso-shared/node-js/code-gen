const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  function tp(name, definition) {
    return (
      log('Type', { prefix, name, definition }) &&
      `${prefix || ''}type ${name} = ${definition}\r\n`
    );
  }

  if (!prefix) {
    tp.Field = (name, type) =>
      log('Type.Field', { name, type }) &&
      `${resolve(name)}: ${resolve(type)};\r\n`;

    tp.Field.Opt = (name, type) => tp.Field(`${name}?`, type);

    tp.Intersection = (...types) =>
      log('Type.Intersection', types) && resolve(types, ' & ');
    tp.Union = (...types) => log('Type.Union', types) && resolve(types, ' | ');
  }

  return tp;
};
