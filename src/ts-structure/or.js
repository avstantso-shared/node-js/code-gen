const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (...members) => {
    return (
      log('Or', { prefix, members }) && `${prefix}${resolve(members, ' || ')}`
    );
  };
};
