const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  function CNS(label, name, typeOrDefinition, definitionIfTyped) {
    return (
      log(label, {
        prefix,
        name,
        ...(definitionIfTyped ? { type: typeOrDefinition } : {}),
        definition: definitionIfTyped || typeOrDefinition,
      }) &&
      `${prefix || ''}const ${resolve(name)}${
        definitionIfTyped ? `: ${resolve(typeOrDefinition)}` : ''
      } = ${resolve(definitionIfTyped || typeOrDefinition).trimEnd()};\r\n`
    );
  }

  function cns(name, typeOrDefinition, definitionIfTyped) {
    return CNS('Const', name, typeOrDefinition, definitionIfTyped);
  }

  cns.Same = (nameAndType, definition) =>
    CNS('Const.Same', nameAndType, nameAndType, definition);

  return cns;
};
