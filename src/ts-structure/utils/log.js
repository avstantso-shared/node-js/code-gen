let enabled = false;

function Log(label, data) {
  if (enabled)
    data ? console.log(`TS.${label}: %O`, data) : console.log(`TS.${label}`);

  return 'ok';
}
Log.on = (condition = undefined) => {
  if (undefined === condition || condition) enabled = true;
};
Log.off = (condition = undefined) => {
  if (undefined === condition || condition) enabled = false;
};

module.exports = Log;
