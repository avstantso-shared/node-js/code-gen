const { resolve, log } = require('./utils');

module.exports =
  (prefix = '') =>
  (begin, end, content) =>
    log('Block', { prefix, begin, end, content }) &&
    `${prefix || ''}${begin}\r\n${`${resolve(
      content
    )}`.trimEnd()}\r\n${end}\r\n`;
