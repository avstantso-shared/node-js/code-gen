const { resolve, log } = require('./utils');
const Block = require('./block');
const Functon = require('./function')();

const block0 = Block();

module.exports = (prefix = '') => {
  const block = Block(prefix);

  function AF(label, begin, end) {
    return (generics, args, returnType, content) =>
      log(label, {
        prefix,
        begin,
        end,
        generics,
        args,
        returnType,
        content,
      }) &&
      block(
        `${generics ? `<${resolve(generics, ',')}>` : ''}(${resolve(
          args,
          ','
        )})${returnType ? `: ${returnType}` : ''} => ${begin}`,
        end,
        content
      );
  }

  function arrowFunc(generics, args, returnType, content) {
    return AF('ArrowFunc', '{', '}')(generics, args, returnType, content);
  }

  arrowFunc.Short = (generics, args, returnType, content) =>
    AF('ArrowFunc.Short', '(', ')')(generics, args, returnType, content);

  // (() => {<content>})()
  arrowFunc.Inline = (content) =>
    log('ArrowFunc.Inline', {
      content,
    }) && Functon.Call(block0('(', ')', arrowFunc('', '', '', content)));

  return arrowFunc;
};
