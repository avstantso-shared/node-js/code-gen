const { resolve, concat, log } = require('./utils');

const And = require('./and')();
const Array = require('./array')();
const ArrowFunc = require('./arrow-func')();
const As = require('./as')();
const Block = require('./block')();
const Comment = require('./comment')();
const Const = require('./const')();
const Export = require('./export')();
const Extends = require('./extends')();
const Function = require('./function')();
const Import = require('./import')();
const Interface = require('./interface')();
const Namespace = require('./namespace')();
const New = require('./new')();
const Object = require('./object')();
const Or = require('./or')();
const Region = require('./region')();
const Return = require('./return')();
const Type = require('./type')();

module.exports = {
  resolve,
  concat,
  log,

  And,
  Array,
  ArrowFunc,
  As,
  Block,
  Call: Function.Call,
  Comment,
  Const,
  Export,
  Extends,
  Function,
  Import,
  Interface,
  Namespace,
  New,
  Object,
  Or,
  Region,
  Return,
  Type,
};
