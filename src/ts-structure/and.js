const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (...members) => {
    return (
      log('And', { prefix, members }) && `${prefix}${resolve(members, ' && ')}`
    );
  };
};
