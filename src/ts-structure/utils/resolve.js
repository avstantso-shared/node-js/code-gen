function Resolve(content, separator = '') {
  return 'function' === typeof content
    ? Resolve(content(), separator)
    : Array.isArray(content)
    ? content
        .map((item) => Resolve(item, separator))
        .filter((item) => item)
        .join(separator)
    : undefined === content || null === content
    ? ''
    : content;
}

Resolve.rn = (content) => Resolve(content, '\r\n');

module.exports = Resolve;
