const { resolve, log } = require('./utils');
const Block = require('./block');

module.exports = (prefix = '') => {
  const block = Block(prefix);

  function func(name, generics, args, returnType, content) {
    return (
      log('Function', { prefix, name, generics, args, returnType, content }) &&
      block(
        `function ${resolve(name)}${
          generics ? `<${resolve(generics)}>` : ''
        }(${resolve(args, ',')})${returnType ? `: ${returnType}` : ''} {`,
        '}',
        content
      )
    );
  }

  if (!prefix) {
    func.Call = (name, args) =>
      log('Function.Call', { name, args }) &&
      `${resolve(name)}(${resolve(args, ',')})`;

    func.Call.Gen = (name, generics, args) => {
      log('Function.Call.Gen', { name, generics, args });
      const n = resolve(name);
      const g = resolve(generics, ',');

      return (
        log('Function.Call.Gen', { name, generics, args }) &&
        func.Call(`${n}${g ? `<${g}>` : ''}`, args)
      );
    };
  }

  return func;
};
