const { resolve, log } = require('./utils');
const Block = require('./block');
const Comment = require('./comment');

const comment = Comment();

module.exports = (prefix = '') => {
  const block = Block(prefix);
  return (name, content) => {
    return (
      log('Region', { prefix, name, content }) &&
      block(
        comment(`#region ${resolve(name)}`).trimEnd(),
        comment('#endregion').trimEnd(),
        content
      )
    );
  };
};
