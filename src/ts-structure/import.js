const { resolve, log } = require('./utils');

function Import(context) {
  const {
    pPrefix = '',
    iPostfix = '',
    cPostfix = '',
    iSeparator = '',
  } = context || {};
  const p = `${pPrefix || ''}import ${iPostfix || ''}`;

  function imp(content) {
    function f() {
      return (
        log('Import', { context, content }) &&
        `${p}${resolve(content, iSeparator)}${cPostfix || ''}`
      );
    }
    f.From = (file) =>
      log('Import.From', { context, content, file }) &&
      `${p}${resolve(content, iSeparator)}${cPostfix || ''} from '${resolve(
        file
      )}';\r\n`;

    return f;
  }

  if (!cPostfix) {
    imp.AsteriskAs = (alias) => imp(`* as ${alias}`);

    imp.Members = Import({
      iPostfix: `${iPostfix || ''}{ `,
      cPostfix: ' }',
      iSeparator: ',',
    });
  }

  if (!context) imp.Type = Import({ iPostfix: 'type ' });

  return imp;
}

module.exports = Import;
