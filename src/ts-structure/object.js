const { resolve, log } = require('./utils');
const Block = require('./block');
const As = require('./as');

module.exports = (prefix = '') => {
  const block = Block(prefix);
  const as = As();

  function obj(content, asType) {
    let r;
    return (
      log('Object', { prefix, content, asType }) &&
      (r = block(`{`, `}`, content)) &&
      (asType ? as(r.trimEnd(), asType) : r)
    );
  }

  if (!prefix) {
    obj.Field = (name, content) =>
      log('Object.Field', { name, content }) &&
      `${resolve(name)}: ${resolve(content).trimEnd()},\r\n`;

    obj.Destr = (content) =>
      log('Object.Destr', { content }) && `...${resolve(content)},\r\n`;
  }

  return obj;
};
