const { resolve, log } = require('./utils');
const Block = require('./block');
const As = require('./as');

module.exports = (prefix = '') => {
  const block = Block(prefix);
  const as = As();

  function arr(content, asType) {
    let r;
    return (
      log('Array', { prefix, content, asType }) &&
      (r = block(`[`, `]`, resolve(content, ','))) &&
      (asType ? as(r.trimEnd(), asType) : r)
    );
  }

  return arr;
};
