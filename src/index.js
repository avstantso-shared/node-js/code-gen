const writeSourceFile = require('./writeSourceFile');
const ScriptHeader = require('./scriptHeader.js');
const TS = require('./ts-structure');
module.exports = { writeSourceFile, ScriptHeader, TS };
