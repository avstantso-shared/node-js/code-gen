const { resolve, log } = require('./utils');
const Block = require('./block');

module.exports = (prefix = '') => {
  const block = Block(prefix);

  function comment(content) {
    return (
      log('Comment', { prefix, content }) &&
      `${prefix || ''}// ${resolve(content)}\r\n`
    );
  }

  comment.Block = (content) =>
    log('Comment.Block', { prefix, content }) && block(`/*`, '*/', content);

  function JSDoc(content) {
    return (
      log('Comment.JSDoc', { prefix, content }) &&
      block(
        `/**`,
        ' */',
        `${resolve(content)}`
          .split('\r\n')
          .map((line) => ` * ${line}`)
          .join('\r\n')
      )
    );
  }

  JSDoc.summary = (content) => `@summary ${resolve(content)}`;
  JSDoc.description = (content) => `@description ${resolve(content)}`;
  JSDoc.returns = (content) => `@returns ${resolve(content)}`;
  JSDoc.param = (content) => `@param ${resolve(content)}`;
  JSDoc.see = (content) => `@see ${resolve(content)}`;

  comment.JSDoc = JSDoc;

  return comment;
};
