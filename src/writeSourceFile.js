const fs = require('fs');
const prettier = require('prettier');

module.exports = async (
  fileName,
  content,
  scriptName,
  options = { format: true, silent: false }
) => {
  try {
    const format =
      !options || undefined === options.format ? true : !!options.format;
    const silent =
      !options || undefined === options.silent ? false : !!options.silent;

    let s = 'function' === typeof content ? content() : content;

    if (scriptName)
      s =
        '// This file generated automaticly by script:\r\n' +
        `// yarn ${scriptName}\r\n` +
        `// at ${new Date().toJSON()}\r\n` +
        '\r\n' +
        s;

    if (format) {
      const prettierConfig = await prettier.resolveConfig(fileName);

      try {
        const formatted = await prettier.format(s, {
          ...prettierConfig,
          filepath: fileName,
        });

        fs.writeFileSync(fileName, formatted);
      } catch (e) {
        fs.writeFileSync(fileName, s);
        throw e;
      }
    } else fs.writeFileSync(fileName, s);

    !silent && console.log(`\x1b[36m+\x1b[0m ${fileName}`);
  } catch (e) {
    e.context = { ...(e.context || {}), fileName };
    throw e;
  }
};
