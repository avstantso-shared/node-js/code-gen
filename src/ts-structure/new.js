const { resolve, log } = require('./utils');

module.exports = (prefix = '') => {
  return (content) => {
    return (
      log('New', { prefix, content }) && `${prefix}new ${resolve(content)}`
    );
  };
};
