module.exports = (scriptName, packagesManager = 'yarn') => ({
  $createdAt: new Date(),
  $createdBy: `${packagesManager} ${scriptName}`,
});
